require('./index.css');
var _mm = require('util/mm.js');

//通用页面头部
var header = {
    init: function () {
        this.onLoad();
        this.bindEvent();
    },
    onLoad: function () {
        var keyword = _mm.getUrlParam('keyword');
        //如果keyword存在，则回填输入框
        if (keyword) {
            $('#search-input').val(keyword);
        }
    },
    bindEvent: function () {
        var _this = this;
        //点击搜索按钮后，在做提交
        $('#search-btn').click(function () {
            _this.searchSubmit();
        });
        //按下回车之后，在做搜索
        $('#search-input').keyup(function (e) {
            //13是回车键的keyCode
            if (e.keyCode === 13) {
                _this.searchSubmit();
            }
        })
    },
    //搜索提交
    searchSubmit: function () {
        var keyword = $.trim($('#search-input').val());
        //如果提交是keyword有值，正常跳转到list页面
        if (keyword) {
            window.location.href = './list.html?keyword=' + keyword;
        }
        //如果keyword为空，直接返回首页
        else {
            _mm.goHome();
        }
    }
};

header.init();